
import HttpPokemonService from '../components/pokemon/service/pokemon.service';

export class FormatPokemonData {
    async reducePokemon() {
        try {
            const { data } = await HttpPokemonService.getInitial();

            const pokemos = await data.results.reduce(
                async (acc, it) => {
                    let accumulator = await acc;
                    const body = await this.formatPokemon(it.url);

                    accumulator = { ...accumulator, [it.name]: body };

                    return accumulator;
                },
                {},
            );

            return pokemos ? Object.values(pokemos) : pokemos;
        } catch (error) { }
    }

    async formatPokemon(url) {
        try {
            const { data: result } = await HttpPokemonService.get(url);

            return this.format(result);
        } catch (error) {
            return undefined;
        }
    }

    format(data) {
        const { moves, sprites, weight, types, id, name } = data;
        return {
            sprites: [
                sprites?.back_default,
                sprites?.back_shiny,
                sprites?.front_default,
                sprites?.front_shiny,
            ],
            moves:
                moves.length > 10 &&
                moves.slice(0, 10).map((m) => m.move?.name),
            weight,
            name,
            types: types.map((t) => t.type?.name),
            img: sprites.other?.home.front_default,
            id,
        }
    }
}
