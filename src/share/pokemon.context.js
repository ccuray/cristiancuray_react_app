import { createContext } from "react";

export const PokemonContext = createContext(null);

export const PokemonSearchContext = createContext(null);