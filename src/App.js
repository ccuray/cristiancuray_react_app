import { useState } from 'react';

import PokemonList from './components/pokemon/list/component.list';
import { PokemonContext, PokemonSearchContext } from './share/pokemon.context';
import './App.css';
import PokemonSearch from './components/pokemon/search/component.search';
import PokemonPreview from './components/pokemon/preview/component.preview';


function App() {
  const [data, setData] = useState({ page: [0, 9], pokemon: {}, search: null })
  const [search, setSearch] = useState({ search: null })
  return (
    <PokemonContext.Provider value={{ data, setData }}>

      <PokemonSearchContext.Provider value={{ search, setSearch }}>
        <PokemonSearch></PokemonSearch>
        <div className="container">

          <div className="item-1">

            <PokemonList></PokemonList>
          </div>
          <div className="item-2">
            <PokemonPreview></PokemonPreview>
          </div>
        </div>
      </PokemonSearchContext.Provider>

    </PokemonContext.Provider>
  );
}

export default App;
