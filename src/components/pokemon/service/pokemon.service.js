import { urlBase } from '../../../configs/rest/urls.enum';
import http from '../../../core/http/http.service';

const getInitial = () => {
    return http.get(`${urlBase.INITIAL_URL}pokemon?limit=60&offset=0`);
};
const getSearch = (search) => {
    try {
        return http.get(`${urlBase.INITIAL_URL}pokemon/${search}`);
    } catch (error) {

    }

};
const get = (url) => {
    return http.get(url);
};

const HttpPokemonService = {
    getInitial,
    get,
    getSearch
};

export default HttpPokemonService;
