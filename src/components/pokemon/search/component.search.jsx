import React, { useContext, useState } from "react";
import { PokemonSearchContext } from "../../../share/pokemon.context";

import "./styles.css";
const PokemonSearch = () => {
    const [searchInput, setSearchInput] = useState();
    const { setSearch } = useContext(PokemonSearchContext);

    const handleInputChange = (event) => {
        const { value } = event.target;
        setSearchInput(value);
        setSearch({ search: value });
    };
    return (
        <div className="searchPanel">
            <input type="text" id="searchInput" placeholder="Search" onChange={handleInputChange} name="searchInput" className="search" />


        </div>
    );
};
export default PokemonSearch;