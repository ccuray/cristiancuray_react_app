import { useContext, useEffect, useState } from 'react';
import { FormatPokemonData } from '../../../middlewares/format.pokemon';
import { PokemonContext, PokemonSearchContext } from '../../../share/pokemon.context';
import HttpPokemonService from '../service/pokemon.service';


import './styles.css';
const PokemonList = () => {
    const formatData = new FormatPokemonData();
    const { data, setData } = useContext(PokemonContext);
    const { search, setSearch } = useContext(PokemonSearchContext);
    const base = [];
    const [pokemons, setPokemons] = useState(base);
    const ramdoColor = () => {

        return '#' + Math.floor(Math.random() * 16777215).toString(16);
    };
    useEffect(() => {
        if (!pokemons.length && !search.search)
            listPokemon();
        if (search?.search)
            searchPokemon()
    }, [search]);
    const searchPokemon = async () => {
        const { data } = await HttpPokemonService.getSearch(search.search);
        if (data) {

            const formatPokemon = await formatData.format(data);
            setPokemons([formatPokemon]);
            setSearch({ search: null });
            setData({ pokemon: formatPokemon })
        }

    }
    const listPokemon = async () => {
        const result = await formatData.reducePokemon();
        result && setPokemons(result);

        setData({ page: [0, 9], pokemon: result[0] },)
    };
    const paguinateResult = () => {


        return data.page ? pokemons.slice(...data.page) : pokemons;
    }
    const clickHandler = (item) => {
        setData({ page: [...data.page], pokemon: item })
    }
    return (
        <div className="grid">
            {!search.search && pokemons.length && paguinateResult().map((item) => (
                <div
                    className="card"
                    style={{ background: ramdoColor() }}
                    key={item.id}
                    onClick={() => clickHandler(item)}
                >
                    <img src={item.img} alt="" width="100" />
                    <h5 className="title">
                        #{item.id} <br />
                        {item.name}
                    </h5>
                </div>


            )) || <div className="loader"></div>}

        </div>
    );
};

export default PokemonList;
