import { useEffect, useState } from "react";
import { useContext } from "react";
import { PokemonContext } from "../../../share/pokemon.context";
import "./styles.css";
const PokemonPreview = () => {
    const { data, setData } = useContext(PokemonContext);
    const [pokemon, setPokemon] = useState();
    useEffect(() => {
        if (data.pokemon)
            setPokemon(data.pokemon)

    }, [data]);
    return (
        <div className="grid-preview" >

            <div className="card-preview">
                <img src={pokemon && pokemon.img} className="preview-img" width="180" />
                <h4 className="title">
                    #{pokemon && pokemon.id} <br />
                    {pokemon && pokemon.name}
                </h4>
                <h4 className="types">
                    <span >
                        Types
                    </span> <br />
                    <span className="sub-text">
                        {pokemon && pokemon.types?.map((item, index) => (
                            <span key={index}> {item} </span>
                        ))}
                    </span>


                </h4>
                <h4 className="peso">
                    <span>
                        Peso
                    </span> <br />
                    <span className="sub-text">
                        {pokemon && pokemon.weight} kg
                    </span>

                </h4>
                <h4 className="sprite">
                    <span >
                        Sprites
                    </span>
                    <br />
                    {pokemon && pokemon.sprites?.map((item, index) => (
                        <img key={index} src={item} width="100" />

                    ))}
                </h4>
                <h4 className="move">
                    <span >
                        Movimientos
                    </span>  <br />
                    <span className="sub-text">
                        {pokemon && pokemon.moves?.map((item, index) => (
                            <span key={index}> {item} </span>

                        ))}
                    </span>

                </h4>
            </div>





        </div>
    )

};

export default PokemonPreview;